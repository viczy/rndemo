'use strict';

import React from 'react';

import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  NavigatorIOS,
} from 'react-native';

var AlarmList = require('./app/view/alarm/alarmList');

var TimeHub = React.createClass({
  render: function() {
    return (
        <AlarmList/>
    );
  },
});

var styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
});

AppRegistry.registerComponent('TimeHub', () => TimeHub);

module.exports = TimeHub;
