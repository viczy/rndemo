'use strict';

import React from 'react';
import Button from 'apsl-react-native-button'

import {
  StyleSheet,
  Image,
  Text,
  View,
  ListView,
  ProgressViewIOS,
} from 'react-native';

var AlarmCell = React.createClass({
  render: function() {
    var data = this.props.data;
    return(
      <View style={styles.container}>
        <Image
          style={styles.cover}
          source={require('../../resource/img/11411438284372_380.jpg')}
        />
        <View style={styles.right_container}>
          <View style={styles.right_top}>
            <Text style={styles.textTitle} numberOfLines={2}>
              {data.title}
            </Text>
          </View>
          <View style={styles.right_middle}>
            <Text style={styles.textPrice}>
              $88
            </Text>
            <Button style={styles.buttonStyle} textStyle={styles.textStyle}
              onPress={() => {
                console.log('world!')
              }}>
              立即抢购
            </Button>
          </View>
          <View style={styles.right_bottom}>
            <ProgressViewIOS style={styles.progressView} progressTintColor="red" progress={0.4}/>
            <Text style={styles.textDescription}>
              剩余40件
            </Text>
          </View>
        </View>
      </View>
    );
  },
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    paddingTop: 6,
    paddingBottom: 6,
  },
  cover: {
    width: 100,
    height: 100,
 },
 right_container: {
   flex: 1,
   flexDirection: 'column',
   marginLeft: 10,
   marginRight: 10,
 },
 right_top : {
   flex: 1,
   paddingTop: 4,
   paddingBottom: 4,
 },
 textTitle: {
   fontSize: 14,
   textAlign: 'left',
 },
 right_middle : {
   flex: 1,
   flexDirection: 'row',
   justifyContent: 'space-between',
   paddingTop: 2,
   paddingBottom: 2,
   alignItems: 'center',
 },
 textPrice : {
   color: 'red',
 },
 textStyle: {
   color: 'white',
   fontSize: 12,
 },
 buttonStyle: {
    borderColor: '#e74c3c',
    backgroundColor: '#e74c3c',
    height: 20,
  },
 right_bottom : {
   flex: 1,
   flexDirection: 'row',
   justifyContent: 'space-between',
   paddingTop: 2,
   paddingBottom: 2,
   alignItems: 'center',
 },
 progressView: {
   width: 160,
   height: 10,
 },
 textDescription : {
   fontSize: 10,
   color: 'gray',
 }
});

module.exports = AlarmCell;
