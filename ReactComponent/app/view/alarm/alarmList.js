'use strict';

import React from 'react';

import {
  StyleSheet,
  Text,
  View,
  ListView,
} from 'react-native';

var AlarmCell = require('../alarm/alarmCell');
// var API_KEY = '7waqfqbprs7pajbz28mqf6vz';
var API_URL = 'http://newsdemo.herokuapp.com/api/v1/infos';
var infoList = [{
  id: 0,
  title: '养生堂 天然维生素E 250mg*90片 +维生素C 850mg*80片 ',
  icon: 'ion-document-text',
  url: '/topic/activities'
}, {
  id: 1,
  title: '养生堂 天然维生素E 250mg*90片 +维生素C 850mg*80片 ',
  icon: 'ion-map',
  url: '/topic/blogs'
}, {
  id: 2,
  title: '养生堂 天然维生素E 250mg*90片 +维生素C 850mg*80片 ',
  icon: 'ion-settings',
  url: '/topic/books'
}, {
  id: 3,
  title: '养生堂 天然维生素E 250mg*90片 +维生素C 850mg*80片 ',
  icon: 'ion-folder',
  url: '/topic/shells'
}, {
  id: 4,
  title: '养生堂 天然维生素E 250mg*90片 +维生素C 850mg*80片 ',
  icon: 'ion-coffee',
  url: '/topic/webs'
}];
// var PAGE_SIZE = 25;
// var PARAMS = '?apikey=' + API_KEY + '&page_limit=' + PAGE_SIZE;
// var REQUEST_URL = API_URL + PARAMS;
var REQUEST_URL = API_URL;

var AlarmList = React.createClass({
  getInitialState: function() {
    return {
      dataSource: new ListView.DataSource({
              rowHasChanged: (row1, row2) => row1 !== row2,
            }),
      loaded: false,
    };
  },

  componentDidMount: function() {
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(infoList),
      loaded: true,
    });
    // this.fetchData();
  },

  fetchData: function() {
    fetch('http://newsdemo.herokuapp.com/api/v1/infos.json')
      .then((response) => response.json())
      .then((responseData) => {
        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(responseData.infos),
          loaded: true,
        });
      })
      .done();
  },

  render: function() {
    if (!this.state.loaded) {
      return this.renderLoadingView();
    }

    return (
      <ListView
        dataSource={this.state.dataSource}
        renderRow={this.renderAlarmCell}
        style={styles.listView}
      />
    );
  },

  renderLoadingView: function() {
    return (
      <View style={styles.container}>
        <Text>
          Loading Data...
        </Text>
      </View>
    );
  },

  renderAlarmCell: function(data) {
    return (
      <AlarmCell
				data={data} />
    );
  },

});


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: '#F5FCFF',
  },
  listView: {
    paddingTop: 0,
    backgroundColor: '#F5FCFF',
  },
});

module.exports = AlarmList;
