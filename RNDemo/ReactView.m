//
//  ReactView.m
//  RNDemo
//
//  Created by 360 on 16/5/23.
//  Copyright © 2016年 RD. All rights reserved.
//

#import "ReactView.h"
#import "RCTRootView.h"
#import <Masonry.h>

@interface ReactView ()

@property (nonatomic, strong) RCTRootView *rootView;

@end

@implementation ReactView

#pragma mark - Life Cycle

- (instancetype)init {
    if (self = [super init]) {
        [self addSubview:self.rootView];
        [self setNeedsUpdateConstraints];
    }
    return self;
}

- (void)updateConstraints {
    [super updateConstraints];
    [self.rootView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}



#pragma mark - Getter

- (RCTRootView *)rootView {
    if (!_rootView) {
        NSURL *jsCodeLocation = [NSURL URLWithString:@"http://localhost:8081/index.ios.bundle?platform=ios"];
//        NSURL *jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
        _rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation moduleName:@"TimeHub" initialProperties:nil launchOptions:nil];
    }
    return _rootView;
}

@end
