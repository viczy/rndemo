//
//  RNOrderListController.m
//  RNDemo
//
//  Created by 360 on 16/5/23.
//  Copyright © 2016年 RD. All rights reserved.
//

#import "RNOrderListController.h"
#import "ReactView.h"
#import <Masonry.h>


@interface RNOrderListController ()

@property (nonatomic, strong) ReactView *reactView;

@end

@implementation RNOrderListController

#pragma mark - Life Cycle

- (void)loadView {
    [super loadView];
    [self.view addSubview:self.reactView];
    [self.reactView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(64, 0, 0, 0));
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"限时抢购";
    self.view.backgroundColor = [UIColor whiteColor];
}


#pragma mark - Getter

- (ReactView *)reactView {
    if (!_reactView) {
        _reactView = [[ReactView alloc] init];
    }
    return _reactView;
}

@end
