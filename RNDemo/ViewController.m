//
//  ViewController.m
//  RNDemo
//
//  Created by 360 on 16/5/23.
//  Copyright © 2016年 RD. All rights reserved.
//

#import "ViewController.h"
#import "RNOrderListController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIButton *orderButton;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"主页";
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Method Private

- (IBAction)orderAction:(id)sender {
    RNOrderListController *orderList = [[RNOrderListController alloc] init];
    [self.navigationController pushViewController:orderList animated:YES];
}


@end
