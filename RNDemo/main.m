//
//  main.m
//  RNDemo
//
//  Created by 360 on 16/5/23.
//  Copyright © 2016年 RD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
