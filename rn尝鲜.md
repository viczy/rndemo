# RN尝鲜
## 环境配置

###Homebrew

`$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`	

### NodeJS
 
`$ brew install node`

### watchman

`$ brew install watchman`

### RN

`$ npm install -g react-native-cli`

## RN项目

`$ react-native init RNDemo`

## 原生项目添加RN模块
### iOS项目

* `$ cd YouriOSProject`
* `$ npm install react-native`
* `$ vim podFile`
* podFile中添加
	
		pod 'React', :path => './node_modules/react-native', :subspecs => [
		  'Core',
		  'RCTImage',
		  'RCTNetwork',
		  'RCTText',
		  'RCTWebSocket',
		  # Add any other subspecs you want to use in your project
		]

* `$ pod install`
* 创建RN模块
	* `$ mkdir ReactComponent`
	* `$ vim ReactComponent/index.ios.js`
	* index.ios.js中添加Demo代码		
	
			'use strict';
			
			import React from 'react';
			import {
			  AppRegistry,
			  StyleSheet,
			  Text,
			  View
			} from 'react-native';
			
			var styles = StyleSheet.create({
			  container: {
			    flex: 1,
			    backgroundColor: 'red'
			  }
			});
			
			class SimpleApp extends React.Component {
			  render() {
			    return (
			      <View style={styles.container}>
			        <Text>This is a simple application.</Text>
			      </View>
			    )
			  }
			}
			
			AppRegistry.registerComponent('SimpleApp', () => SimpleApp);
	* 添加RN的适配控件
		
			// ReactView.h
			
			#import <UIKit/UIKit.h>
			@interface ReactView : UIView
			@end
			
* `$ (JS_DIR='pwd'/ReactComponent; cd node_modules/react-native; npm run start -- --root $JS_DIR)` 启动React Native开发服务器

## RN-Flex布局

## RN－Flux架构：Redux

####参考资料
* <http://www.ruanyifeng.com/blog/2015/07/flex-grammar.html>
* <http://www.ruanyifeng.com/blog/2015/07/flex-examples.html>
* <http://www.ruanyifeng.com/blog/2016/01/flux.html>
* <http://cn.redux.js.org/docs/advanced/Middleware.html>
* <http://bbs.reactnative.cn/topic/725/code-push-热更新使用详细说明和教程>
* <https://facebook.github.io/react-native/docs/embedded-app-ios.html#content>
* <http://reactnative.cn/docs/0.26/embedded-app-ios.html#content>
* <https://segmentfault.com/a/1190000004352162>
